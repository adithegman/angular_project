import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ClientService} from "../shared/client.service";

@Component({
  selector: 'app-clients-new',
  templateUrl: './clients-new.component.html',
  styleUrls: ['./clients-new.component.css']
})
export class ClientsNewComponent implements OnInit {

  constructor(private clientService: ClientService,
              private router: Router) { }

  ngOnInit(): void {
  }

  save(firstName : string, lastName : string, address : string): void {
    let Client = {id:0, firstName, lastName, address};

    this.clientService.saveClient(Client)
      .subscribe(res => { console.log("Saved client: ", res);
                              this.router.navigate(['clients'])});

  }

}
