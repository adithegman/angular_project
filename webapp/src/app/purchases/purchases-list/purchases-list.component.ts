import { Component, OnInit } from '@angular/core';
import {Purchase} from "../shared/purchase.model";
import {PurchaseService} from "../shared/purchase.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-purchases-list',
  templateUrl: './purchases-list.component.html',
  styleUrls: ['./purchases-list.component.css']
})
export class PurchasesListComponent implements OnInit {
  errorMessage: string;
  purchases: Array<Purchase>;
  selectedPurchase: Purchase;

  constructor(private purchaseService: PurchaseService,
              private router: Router) { }

  ngOnInit(): void {
    this.getPurchases();
  }

  getPurchases(): void {
    this.purchaseService.getPurchases()
      .subscribe(
        purchases => this.purchases = purchases,
        error => this.errorMessage = <any>error
      );
  }

  onSelect(purchase: Purchase): void {
    this.selectedPurchase = purchase;
  }

  addNewPurchase(): void {
    console.log("Redirecting to purchases/new");
    this.router.navigate(["purchases/new"]);
  }

  deletePurchase(purchase: Purchase): void {
    this.purchaseService.deletePurchase(purchase.id)
      .subscribe(p => {
        console.log("deleted purchase response: ", p);
        this.getPurchases(); // refresh
      })
  }
}
