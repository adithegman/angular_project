import {Component, Input, OnInit} from '@angular/core';
import {Book} from "../shared/book.model";
import {BookService} from "../shared/book.service";

@Component({
  selector: 'app-books-detail',
  templateUrl: './books-detail.component.html',
  styleUrls: ['./books-detail.component.css']
})
export class BooksDetailComponent implements OnInit {
  @Input() book : Book;
  constructor(private bookService: BookService) { }

  ngOnInit(): void {
  }

  save() : void {
    this.bookService.updateBook(this.book).subscribe(book => console.log("updated book: ", book));
  }
}
