import { Component, OnInit } from '@angular/core';
import {BookService} from "../shared/book.service";
import {Book} from "../shared/book.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-books-new',
  templateUrl: './books-new.component.html',
  styleUrls: ['./books-new.component.css']
})
export class BooksNewComponent implements OnInit {

  constructor(private bookService: BookService,
              private router: Router) { }

  ngOnInit(): void {
  }

  save(title : string, author : string, year : string, price : string): void {
    let Book = {id:0, title, author, year:Number(year), price:Number(price)};

    this.bookService.saveBook(Book)
      .subscribe(res => { console.log("Saved book: ", res);
                              this.router.navigate(['books'])});
  }

}
