import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {PurchaseService} from "../shared/purchase.service";

@Component({
  selector: 'app-purchases-new',
  templateUrl: './purchases-new.component.html',
  styleUrls: ['./purchases-new.component.css']
})
export class PurchasesNewComponent implements OnInit {

  constructor(private purchaseService: PurchaseService,
              private router: Router) { }

  ngOnInit(): void {
  }

    save(client: string, book: string, date: string): void {
    let Purchase = {id: 0, book: Number(book), client: Number(client), transactionDate: date};

    this.purchaseService.savePurchase(Purchase)
      .subscribe(res => {
        console.log("Saved purchase: ", res);
        this.router.navigate(["purchases"]);
      })
  }
}
