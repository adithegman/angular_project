package ro.ubb.bookstore.common.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.bookstore.common.model.Purchase;
import ro.ubb.bookstore.common.repository.PurchaseRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PurchaseServiceImpl implements PurchaseService {
    public static final Logger log = LoggerFactory.getLogger(PurchaseServiceImpl.class);

    @Autowired
    PurchaseRepository purchaseRepository;

    @Override
    public List<Purchase> getAllPurchases() {
        log.trace("getAllPurchases --- Method called");
        List<Purchase> result = purchaseRepository.findAll();
        log.trace("getAllPurchases: result={}", result);

        return result;
    }

    @Override
    public Purchase savePurchase(Purchase purchase) {
        log.trace("savePurchase --- Method called");
        Purchase result = purchaseRepository.save(purchase);
        log.trace("savePurchase: saved purchase {}", result);

        return result;
    }

    @Override
    @Transactional
    public Purchase updatePurchase(Long id, Purchase purchase) {
        log.trace("updatePurchase --- Method called");
        Purchase result = purchaseRepository.findById(id).orElse(purchase);
        result.setBook(purchase.getBook());
        result.setClient(purchase.getClient());
        result.setTransactionDate(purchase.getTransactionDate());
        log.trace("updatePurchase: updated purchase {}", result);

        return result;
    }

    @Override
    public void deletePurchase(Long id) {
        log.trace("deletePurchase --- Method called");
        Optional<Purchase> deleted = purchaseRepository.findById(id);
        purchaseRepository.deleteById(id);

        if(deleted.isEmpty())
            log.trace("deletePurchase: no purchase deleted");
        else
            log.trace("deletePurchase: deleted purchase {}", deleted.get());

    }

    @Override
    public void deletePurchasesOfBook(Long id) {
        log.trace("deletePurchasesOfBook --- Method called");
        List<Purchase> allPurchases = this.getAllPurchases();

        List<Purchase> toDelete = allPurchases.stream().filter(purchase -> purchase.getBook().equals(id))
                .collect(Collectors.toList());

        log.trace("toDelete: purchases that contain the Book:\n{}", toDelete);
        toDelete.forEach(purchase -> this.deletePurchase(purchase.getId()));
    }

    @Override
    public void deletePurchasesOfClient(Long id) {
        log.trace("deletePurchasesOfClient --- Method called");
        List<Purchase> allPurchases = this.getAllPurchases();

        List<Purchase> toDelete = allPurchases.stream().filter(purchase -> purchase.getClient().equals(id))
                .collect(Collectors.toList());

        log.trace("toDelete: purchases that contain the Client:\n{}", toDelete);
        toDelete.forEach(purchase -> this.deletePurchase(purchase.getId()));
    }
}
