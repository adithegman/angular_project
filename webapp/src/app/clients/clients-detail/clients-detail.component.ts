import {Component, Input, OnInit} from '@angular/core';
import {Client} from "../shared/client.model";
import {ClientService} from "../shared/client.service";

@Component({
  selector: 'app-clients-detail',
  templateUrl: './clients-detail.component.html',
  styleUrls: ['./clients-detail.component.css']
})
export class ClientsDetailComponent implements OnInit {
  @Input() client: Client;
  constructor(private clientService: ClientService) { }

  ngOnInit(): void {
  }

  save() : void {
    this.clientService.updateClient(this.client).subscribe(client => console.log("updated client: ", client));
  }

}
