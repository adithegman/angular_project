package ro.ubb.bookstore.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.bookstore.common.service.ClientService;
import ro.ubb.bookstore.common.service.PurchaseService;
import ro.ubb.bookstore.web.converter.ClientConverter;
import ro.ubb.bookstore.web.dto.ClientDto;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ClientController {
    public static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    List<ClientDto> getClients() {
        log.trace("getClients --- called");

        return new ArrayList<>(clientConverter.convertModelsToDtos(clientService.getAllClients()));
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto clientDto) {
        log.trace("saveClient --- called");

        return clientConverter.convertModelToDto(
                clientService.saveClient(clientConverter.convertDtoToModel(clientDto)));
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto updateClient(@PathVariable Long id, @RequestBody ClientDto clientDto) {
        log.trace("updateClient --- Called");

        return clientConverter.convertModelToDto(
                clientService.updateClient(id, clientConverter.convertDtoToModel(clientDto)));
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable Long id) {
        log.trace("deleteClient --- Called");

        purchaseService.deletePurchasesOfClient(id);
        clientService.deleteClient(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
