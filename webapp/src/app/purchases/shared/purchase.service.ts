import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Purchase} from "./purchase.model";

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {
  private purchaseURL = 'http://localhost:8080/api/purchases';
  constructor(private httpClient: HttpClient) { }

  getPurchases(): Observable<Purchase[]> {
    return this.httpClient
      .get<Array<Purchase>>(this.purchaseURL);
  }

  savePurchase(purchase: Purchase): Observable<Purchase> {
    console.log("savePurchase: ", purchase);
    return this.httpClient.post<Purchase>(this.purchaseURL, purchase);
  }

  updatePurchase(purchase: Purchase): Observable<Purchase> {
    const url = `${this.purchaseURL}/${purchase.id}`;
    console.log("updatePurchase: ", purchase);
    console.log("updateUrl: ", url);
    return this.httpClient.put<Purchase>(url, purchase);
  }

  deletePurchase(id: number): Observable<any> {
    const url = `${this.purchaseURL}/${id}`;
    console.log("deletePurchase with ID ", id);
    console.log("deleteUrl: ", url);
    return this.httpClient.delete(url);
  }
}
