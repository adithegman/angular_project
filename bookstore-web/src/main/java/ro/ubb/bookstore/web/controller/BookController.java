package ro.ubb.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.bookstore.common.service.BookService;
import ro.ubb.bookstore.common.service.PurchaseService;
import ro.ubb.bookstore.web.converter.BookConverter;
import ro.ubb.bookstore.web.dto.BookDto;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {
    public static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private BookConverter bookConverter;

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    List<BookDto> getBooks() {
        log.trace("getBooks --- called");
        return new ArrayList<>(bookConverter.convertModelsToDtos(bookService.getAllBooks()));
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    BookDto saveBook(@RequestBody BookDto bookDto) {
        log.trace("BookController.saveBook --- called");
        return bookConverter.convertModelToDto(bookService.saveBook(
                bookConverter.convertDtoToModel(bookDto)
        ));
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.PUT)
    BookDto updateBook(@PathVariable Long id, @RequestBody BookDto bookDto) {
        log.trace("updateBook --- called");
        return bookConverter.convertModelToDto(bookService.updateBook(
                id, bookConverter.convertDtoToModel(bookDto)
        ));
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteBook(@PathVariable Long id) {
        log.trace("deleteBook --- called");
        purchaseService.deletePurchasesOfBook(id);
        bookService.deleteBook(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
