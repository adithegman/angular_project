package ro.ubb.bookstore.common.service;

import ro.ubb.bookstore.common.model.Purchase;

import java.util.List;

public interface PurchaseService {
    List<Purchase> getAllPurchases();
    Purchase savePurchase(Purchase purchase);
    Purchase updatePurchase(Long id, Purchase purchase);
    void deletePurchase(Long id);
    void deletePurchasesOfBook(Long id);
    void deletePurchasesOfClient(Long id);
}
