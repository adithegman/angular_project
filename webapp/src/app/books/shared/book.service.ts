import {Injectable} from '@angular/core';

import {HttpClient, HttpResponse} from "@angular/common/http";

import {Observable} from "rxjs";
import {Book} from "./book.model";


@Injectable()
export class BookService {
  private booksUrl = 'http://localhost:8080/api/books';

  constructor(private httpClient: HttpClient) {
  }

  getBooks(): Observable<Book[]> {
    return this.httpClient
      .get<Array<Book>>(this.booksUrl);
  }

  saveBook(book: Book): Observable<Book> {
    console.log("saveBook: ", book);
    return this.httpClient.post<Book>(this.booksUrl, book);
  }

  updateBook(book: Book): Observable<Book> {
    const url = `${this.booksUrl}/${book.id}`;
    console.log("updateBook: ", book);
    console.log("updateUrl: ", url);
    return this.httpClient.put<Book>(url, book);
  }

  deleteBook(id: number): Observable<any> {
    const url = `${this.booksUrl}/${id}`;
    console.log("deleteBook with ID ", id);
    console.log("deleteUrl: ", url);
    return this.httpClient.delete(url);
  }

}
