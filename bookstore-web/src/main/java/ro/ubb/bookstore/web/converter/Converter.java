package ro.ubb.bookstore.web.converter;

import ro.ubb.bookstore.common.model.BaseEntity;
import ro.ubb.bookstore.web.dto.BaseDto;

/**
 * Created by radu.
 */

public interface Converter<Model extends BaseEntity<Long>, Dto extends BaseDto> {

    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);

}

