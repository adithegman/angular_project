import {Component, Input, OnInit} from '@angular/core';
import {Purchase} from "../shared/purchase.model";
import {PurchaseService} from "../shared/purchase.service";

@Component({
  selector: 'app-purchases-detail',
  templateUrl: './purchases-detail.component.html',
  styleUrls: ['./purchases-detail.component.css']
})
export class PurchasesDetailComponent implements OnInit {

  @Input() purchase: Purchase;
  constructor(private purchaseService: PurchaseService) { }

  ngOnInit(): void {
  }

  save() : void {
    this.purchaseService.updatePurchase(this.purchase)
      .subscribe(p => console.log("updated purchase: ", p))
  }
}
