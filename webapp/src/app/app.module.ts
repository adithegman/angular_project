import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BooksListComponent } from './books/books-list/books-list.component';
import { BookService } from "./books/shared/book.service";
import { HttpClientModule } from "@angular/common/http";
import { BooksDetailComponent } from './books/books-detail/books-detail.component';
import { FormsModule } from '@angular/forms';
import { BooksNewComponent } from './books/books-new/books-new.component';
import { ClientsComponent } from './clients/clients.component';
import { ClientsListComponent } from './clients/clients-list/clients-list.component';
import {ClientService} from "./clients/shared/client.service";
import { ClientsDetailComponent } from './clients/clients-detail/clients-detail.component';
import { ClientsNewComponent } from './clients/clients-new/clients-new.component';
import { PurchasesComponent } from './purchases/purchases.component';
import { PurchasesListComponent } from './purchases/purchases-list/purchases-list.component';
import {PurchaseService} from "./purchases/shared/purchase.service";
import { PurchasesDetailComponent } from './purchases/purchases-detail/purchases-detail.component';
import { PurchasesNewComponent } from './purchases/purchases-new/purchases-new.component';

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    BooksListComponent,
    BooksDetailComponent,
    BooksNewComponent,
    ClientsComponent,
    ClientsListComponent,
    ClientsDetailComponent,
    ClientsNewComponent,
    PurchasesComponent,
    PurchasesListComponent,
    PurchasesDetailComponent,
    PurchasesNewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [BookService, ClientService, PurchaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
