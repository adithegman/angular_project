import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksNewComponent } from "./books/books-new/books-new.component";
import {BooksComponent} from "./books/books.component";
import {ClientsComponent} from "./clients/clients.component";
import {ClientsNewComponent} from "./clients/clients-new/clients-new.component";
import {PurchasesComponent} from "./purchases/purchases.component";
import {PurchasesNewComponent} from "./purchases/purchases-new/purchases-new.component";



const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'books', component: BooksComponent},
  {path: 'books/new', component: BooksNewComponent},
  {path: 'clients', component: ClientsComponent},
  {path: 'clients/new', component: ClientsNewComponent},
  {path: 'purchases', component: PurchasesComponent},
  {path: 'purchases/new', component: PurchasesNewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
