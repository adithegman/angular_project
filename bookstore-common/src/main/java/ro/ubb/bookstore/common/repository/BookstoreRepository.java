package ro.ubb.bookstore.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ro.ubb.bookstore.common.model.BaseEntity;

import java.io.Serializable;

@NoRepositoryBean
public interface BookstoreRepository <T extends BaseEntity<ID>, ID extends Serializable>
        extends JpaRepository<T, ID> {
}
