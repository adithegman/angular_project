import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Client} from "../shared/client.model";
import {ClientService} from "../shared/client.service";

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})
export class ClientsListComponent implements OnInit {

  errorMessage: string;
  clients: Array<Client>;
  selectedClient: Client;

  constructor(private clientService: ClientService, private router: Router) { }

  ngOnInit(): void {
    this.getClients();
  }

  getClients(): void {
    this.clientService.getClients()
      .subscribe(
        clients => this.clients = clients,
        error => this.errorMessage = <any>error
      );
  }

  onSelect(client: Client): void {
    this.selectedClient = client;
  }

  addNewClient(): void {
    console.log("Redirecting to clients/new");
    this.router.navigate(["clients/new"]);
  }

  deleteClient(client: Client): void {
    this.clientService.deleteClient(client.id)
      .subscribe(client => { console.log("deleted client response: ", client);
                                  this.getClients() });
  }
}
