package ro.ubb.bookstore.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.bookstore.common.service.BookService;
import ro.ubb.bookstore.common.service.ClientService;
import ro.ubb.bookstore.common.service.PurchaseService;
import ro.ubb.bookstore.web.converter.PurchaseConverter;
import ro.ubb.bookstore.web.dto.ClientDto;
import ro.ubb.bookstore.web.dto.PurchaseDto;
import ro.ubb.bookstore.web.dto.PurchasesDto;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PurchaseController {
    public static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private BookService bookService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private PurchaseConverter purchaseConverter;

    @RequestMapping(value = "/purchases", method = RequestMethod.GET)
    List<PurchaseDto> getPurchases() {
        log.trace("getPurchases --- called");

        return new ArrayList<>(purchaseConverter.convertModelsToDtos(
                purchaseService.getAllPurchases()));
    }

    @RequestMapping(value = "/purchases", method = RequestMethod.POST)
    PurchaseDto savePurchase(@RequestBody PurchaseDto purchaseDto) {
        log.trace("savePurchase --- called");

        if(clientService.getClient(purchaseDto.getClient()).isEmpty() ||
           bookService.getBook(purchaseDto.getBook()).isEmpty())
        {
            PurchaseDto ret = new PurchaseDto();
            ret.setId((long)-1);

            return ret;
        }

        return purchaseConverter.convertModelToDto(
                purchaseService.savePurchase(purchaseConverter.convertDtoToModel(purchaseDto))
        );
    }


    @RequestMapping(value = "/purchases/{id}", method = RequestMethod.PUT)
    PurchaseDto updatePurchase(@PathVariable Long id, @RequestBody PurchaseDto purchaseDto) {
        log.trace("updatePurchase --- Called");

        return purchaseConverter.convertModelToDto(
                purchaseService.updatePurchase(id, purchaseConverter.convertDtoToModel(purchaseDto)));
    }

    @RequestMapping(value = "/purchases/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deletePurchase(@PathVariable Long id) {
        log.trace("deletePurchase --- Called");

        purchaseService.deletePurchase(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
