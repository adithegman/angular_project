import {Injectable} from '@angular/core';

import {HttpClient, HttpResponse} from "@angular/common/http";

import {Observable} from "rxjs";
import {Client} from "./client.model";

@Injectable()
export class ClientService {
  private clientsURL = 'http://localhost:8080/api/clients';

  constructor(private httpClient: HttpClient) {
  }

  getClients(): Observable<Client[]> {
    return this.httpClient
      .get<Array<Client>>(this.clientsURL);
  }

  saveClient(client: Client): Observable<Client> {
    console.log("saveClient: ", client);
    return this.httpClient.post<Client>(this.clientsURL, client);
  }

  updateClient(client: Client): Observable<Client> {
    const url = `${this.clientsURL}/${client.id}`;
    console.log("updateClient: ", client);
    console.log("updateUrl: ", url);
    return this.httpClient.put<Client>(url, client);
  }


  deleteClient(id: number): Observable<any> {
    const url = `${this.clientsURL}/${id}`;
    console.log("deleteClient with ID ", id);
    console.log("deleteUrl: ", url);
    return this.httpClient.delete(url);
  }

}
