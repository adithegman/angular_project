package ro.ubb.bookstore.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.bookstore.common.model.Purchase;
import ro.ubb.bookstore.web.dto.PurchaseDto;

@Component
public class PurchaseConverter extends BaseConverter<Purchase, PurchaseDto> {
    @Override
    public Purchase convertDtoToModel(PurchaseDto dto) {
        Purchase purchase = Purchase.builder()
                .book(dto.getBook())
                .client(dto.getClient())
                .transactionDate(dto.getTransactionDate())
                .build();
        purchase.setId(dto.getId());
        return purchase;
    }

    @Override
    public PurchaseDto convertModelToDto(Purchase purchase) {
        PurchaseDto dto = PurchaseDto.builder()
                .book(purchase.getBook())
                .client(purchase.getClient())
                .transactionDate(purchase.getTransactionDate())
                .build();

        dto.setId(purchase.getId());
        return dto;
    }
}
