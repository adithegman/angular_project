import { Component, OnInit } from '@angular/core';
import {Book} from "../shared/book.model";
import {BookService} from "../shared/book.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.css']
})
export class BooksListComponent implements OnInit {
  /*
  books: Book[] = [
    {id: 1, title: 'Title1', author: 'Author1', year: 1999, price: 10},
    {id: 2, title: 'Title2', author: 'Author2', year: 1991, price: 20},
    {id: 3, title: 'Title3', author: 'Author3', year: 1929, price: 30},
    {id: 4, title: 'Title4', author: 'Author4', year: 1881, price: 40}
  ];*/
  errorMessage: string;
  books: Array<Book>;
  selectedBook: Book;

  constructor(private bookService: BookService, private router:Router) {
  }

  ngOnInit(): void {
    this.getBooks()
  }

  addNewBook(): void {
    console.log("Redirecting to book/new");
    this.router.navigate(["books/new"]);
  }

  getBooks(): void {
    this.bookService.getBooks()
      .subscribe(
        books => this.books = books,
        error => this.errorMessage = <any>error
      );
  }

  onSelect(book: Book): void {
    this.selectedBook = book;
  }

  deleteBook(book: Book): void {
    this.bookService.deleteBook(book.id)
      .subscribe(book => { console.log("deleted book response: ", book);
                                this.getBooks() });
  }
}
