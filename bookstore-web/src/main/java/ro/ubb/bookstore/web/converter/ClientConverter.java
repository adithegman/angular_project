package ro.ubb.bookstore.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.bookstore.common.model.Client;
import ro.ubb.bookstore.web.dto.ClientDto;

@Component
public class ClientConverter extends BaseConverter<Client, ClientDto> {
    @Override
    public Client convertDtoToModel(ClientDto dto) {
        Client client = Client.builder()
                .address(dto.getAddress())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .build();

        client.setId(dto.getId());
        return client;
    }

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto dto = ClientDto.builder()
                .address(client.getAddress())
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .build();

        dto.setId(client.getId());
        return dto;
    }
}